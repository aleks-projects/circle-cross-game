import pygame

pygame.init()


def winner(lista: list, znak: str):
    zeroes = 0
    for row in lista:
        zeroes += row.count(0)
        if row.count(znak) == 3:
            return znak
    for col in range(3):
        if lista[0][col] == znak and lista[1][col] == znak and lista[2][col] == znak:
            return znak
    if lista[0][0] == znak and lista[1][1] == znak and lista[2][2] == znak:
        return znak
    if lista[0][2] == znak and lista[1][1] == znak and lista[2][0] == znak:
        return znak
    if zeroes == 0:
        return 'Remis'
    return False


kratka = 100
odstep = 15
width = height = kratka * 3 + odstep * 4

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
checker = [[0] * 3 for i in range(3)]

size = (width, height)
screen = pygame.display.set_mode(size)
pygame.display.set_caption('Kolko krzyzyk')
kroki = 0
game_over = False

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
        elif event.type == pygame.MOUSEBUTTONDOWN and not game_over:
            x_mouse, y_mouse = pygame.mouse.get_pos()
            column = x_mouse // (odstep + kratka)
            row = y_mouse // (odstep + kratka)
            if checker[row][column] == 0:
                if kroki % 2 == 0:
                    checker[row][column] = 'x'
                else:
                    checker[row][column] = 'o'
                kroki += 1
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:

            game_over = False
            checker = [[0] * 3 for i in range(3)]
            kroki = 0
            screen.fill(BLACK)

    if not game_over:
        for col in range(3):
            for row in range(3):
                if checker[row][col] == 'x':
                    color = RED
                elif checker[row][col] == 'o':
                    color = GREEN
                else:
                    color = WHITE
                x = col * kratka + (col + 1) * odstep
                y = row * kratka + (row + 1) * odstep
                pygame.draw.rect(screen, color, (x, y, kratka, kratka))
                if color == RED:
                    pygame.draw.line(screen, WHITE, (x + 5, y + 5), (x + kratka - 5, y + kratka - 5), 3)
                    pygame.draw.line(screen, WHITE, (x + kratka - 5, y + 5), (x + 5, y + kratka - 5), 3)
                elif color == GREEN:
                    pygame.draw.circle(screen, WHITE, (x + kratka // 2, y + kratka // 2), kratka // 2 - 3, 3)

    if (kroki - 1) % 2 == 0:

        game_over = winner(checker, 'x')
    else:
        game_over = winner(checker, 'o')

    if game_over:
        screen.fill(BLACK)
        font = pygame.font.SysFont('stxingkai', 150)
        text1 = font.render(game_over, True, WHITE)
        text_rect = text1.get_rect()
        text_x = screen.get_width() / 2 - text_rect.width / 2
        text_y = screen.get_height() / 2 - text_rect.height / 2
        screen.blit(text1, [text_x, text_y])

    pygame.display.update()